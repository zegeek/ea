var options = {
    id: 'color-bar',
    target: document.getElementById('progress-bar')
};
var bar = new Nanobar( options );
function progressBar(color,shadow) {
    var colorBar = document.getElementById('color-bar');
    var Bar = null;
    for (var i = 0; i < colorBar.childNodes.length; i++) {
        if (colorBar.childNodes[i].className == "bar") {
            Bar = colorBar.childNodes[i];
            break;
        }        
    }
    Bar.style.backgroundColor = color;
    Bar.style.boxShadow = shadow;
    bar.go(100);
}
var timerBar = true;
$('#nav-item-ul .nav-item').click(function(){
    var myColor = $(this).attr('color');
    if (timerBar) {
        timerBar = false;
        progressBar(myColor,'0 0 10px '+myColor);
    }
    setTimeout(function(){
        timerBar = true;
    },1000);
});
$(document).ready(function(){
    $('.search-option').click(function() {
        var new_bar;
        if($(this).attr('id') == 'produit-option') {
            new_bar = $('#produit-navigation');
        }else if($(this).attr('id') == 'solution-option') {
            new_bar = $('#solution-navigation');
        }else {
            new_bar = $('#marque-navigation');
        }
        $('#lower-navigation').transition({
            x: '100%'},1000,'ease');
        new_bar.transition({
            x: '0'},1000,'ease');
        $('.selection-item').show();
    });
    $('.rechercher-par').click(function() {
        $('.selection-item').transition({
                top: '150%'
            }).hide();
        $(this).parent().parent().parent().transition({
            x: '-100%'},1000,'ease');
        $('#lower-navigation').transition({
            x: '0'},1000,'ease');
    });
    $('#main-slider .item').css("background-image",function() {
        return "url('"+$(this).attr('image')+"')";
    });
    $('.my-slider').slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        dots: true
    });
    $('.lang-choose').mouseover(function() {
        if($(this).attr('id') == "fr-link") {
            $(this).stop().animate({
                color: "#FF0000"}, 100);
        }
        else
        {
            $(this).stop().animate({
                color: "#1010FF"}, 100);
        }
    }).mouseout(function() {
        $(this).stop().animate({
            color: "#CCCCCC"},100);
    });
    $('.nav-link').mouseover(function() {
        $(this).stop().animate({
            "paddingTop" : "25px"},100);
    }).mouseout(function() {
        $(this).stop().animate({
            "paddingTop" : "15px"},100);
    });
    $('.vid-control').mouseover(function() {
        $(this).children('img').stop().animate({
            width : "25px",
            height : "25px" },100);
    }).mouseout(function() {
        $(this).children('img').stop().animate({
            width : "22px",
            height : "22px" },100);
    });
    var timer, rot='180';
    $('.lamp-button').hover(function() {
        rot = (rot == '0') ? '180' : '0' ;
        var color = $(this).parent().parent().attr('color');
        var item = $(this).parent().parent().attr('item');
        var bar_ = $('#'+item);
        $(this).children('.lamp-text').stop().transition({ y: '60px'});
        $(this).children('img').stop().transition({ scale: 1.1 });
        $(this).stop().transition({ "border-bottom" : "15px solid #"+color, "background-color" : "#FFFFF" });
        if(bar_.attr('hide') == "yes") {    
            bar_.attr('hide','no').transition({
                top: '92.5%'
            });
        }else {
            bar_.transition({
                rotateY: rot+'deg'

            },500,'cubic-bezier(0.455, 0.030, 0.515, 0.955)');
        }
    },function() {
        var color = $(this).parent().parent().attr('color');
        var item = $(this).parent().parent().attr('item');
        var bar_ = $('#'+item);
        $(this).children('.lamp-text').stop().transition({ y: '0px'});
        $(this).children('img').stop().transition({ scale: 1.0 });
        $(this).stop().transition({ "border-bottom" : "solid 0px #"+color, "background-color" : "transparent" });
        // bar_.css('transform','initial');
    }).mousemove(function() {
        var color = $(this).parent().parent().attr('color');
        var item = $(this).parent().parent().attr('item');
        var bar_ = $('#'+item);
        if (timer) {
            clearTimeout(timer);
            timer = 0;
        }
        if(bar_.attr('hide') == "yes") {    
            bar_.stop().transition({
                top: '92.5%'
            });
            bar_.attr('hide','no');
        }
        timer = setTimeout(function() {
            bar_.transition({
                top: '150%'
            });
            bar_.attr('hide','yes');
        }, 2000);
    });
    $('.selection-item').mousemove(function () {
        var bar_ = $(this);
        if (timer) {
            clearTimeout(timer);
            timer = 0;
        }
        if($(this).attr('hide') == "yes") {    
            bar_.transition({
                top: '92.5%'
            });
            bar_.attr('hide','no');
        }
        timer = setTimeout(function() {
            bar_.transition({
                top: '150%'
            });
            bar_.attr('hide','yes');
        }, 2000);
    });
    $('#lower-navigation,.selection-item').hover(function(){},function() {
        var bar_ = $('.selection-item');
        bar_.transition({
                top: '150%'
            });
        bar_.attr('hide','yes');
    });
    $('.item-in-selection').mouseover(function() {
        $(this).children('img').stop().transition({ scale: 1.12 });
    }).mouseout(function() {
        $(this).children('img').stop().transition({ scale: 1 });
    });
    $('#search-button').click(function() {
        $(this).children('.fa-angle-right').css({

        //for firefox
        "-moz-animation-name":"rotatebox",
        "-moz-animation-duration":"0.8s",
        "-moz-animation-iteration-count":"1",
        "-moz-animation-fill-mode":"forwards",

        //for safari & chrome
        "-webkit-animation-name":"rotatebox",
        "-webkit-animation-duration":"0.8s",
        "-webkit-animation-iteration-count":"1",
        "-webkit-animation-fill-mode" : "forwards",

    });
    });
});
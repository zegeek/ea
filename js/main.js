jQuery(function($) {'use strict';

	// Navigation Scroll
	$(window).scroll(function(event) {
		Scroll();
	});

	$('.navbar-collapse ul li a').on('click', function() {  
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// User define function
	function Scroll() {
		var contentTop      =   [];
		var contentBottom   =   [];
		var winTop      =   $(window).scrollTop();
		var rangeTop    =   200;
		var rangeBottom =   500;
		$('.navbar-collapse').find('.scroll a').each(function(){
			contentTop.push( $( $(this).attr('href') ).offset().top);
			contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
		})
		$.each( contentTop, function(i){
			if ( winTop > contentTop[i] - rangeTop ){
				$('.navbar-collapse li.scroll')
				.removeClass('active')
				.eq(i).addClass('active');			
			}
		})
	};

	$('#tohash').on('click', function(){
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Slider
	$(document).ready(function() {
		var time = 7; // time in seconds

	 	var $progressBar,
	      $bar, 
	      $elem, 
	      isPause, 
	      tick,
	      percentTime;
	 
	    //Init the carousel
	    $("#main-slider").find('.owl-carousel').owlCarousel({
	      slideSpeed : 500,
	      paginationSpeed : 500,
	      singleItem : true,
	      navigation : true,
			navigationText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"
			],
	      afterInit : progressBar,
	      afterMove : moved,
	      startDragging : pauseOnDragging,
	      //autoHeight : true,
	      transitionStyle : "fadeUp"
	    });
	 
	    //Init progressBar where elem is $("#owl-demo")
	    function progressBar(elem){
	      $elem = elem;
	      //build progress bar elements
	      buildProgressBar();
	      //start counting
	      start();
	    }
	 
	    //create div#progressBar and div#bar then append to $(".owl-carousel")
	    function buildProgressBar(){
	      $progressBar = $("<div>",{
	        id:"progressBar"
	      });
	      $bar = $("<div>",{
	        id:"bar"
	      });
	      $progressBar.append($bar).appendTo($elem);
	    }
	 
	    function start() {
	      //reset timer
	      percentTime = 0;
	      isPause = false;
	      //run interval every 0.01 second
	      tick = setInterval(interval, 10);
	    };
	 
	    function interval() {
	      if(isPause === false){
	        percentTime += 1 / time;
	        $bar.css({
	           width: percentTime+"%"
	         });
	        //if percentTime is equal or greater than 100
	        if(percentTime >= 100){
	          //slide to next item 
	          $elem.trigger('owl.next')
	        }
	      }
	    }
	 
	    //pause while dragging 
	    function pauseOnDragging(){
	      isPause = true;
	    }
	 
	    //moved callback
	    function moved(){
	      //clear interval
	      clearTimeout(tick);
	      //start again
	      start();
	    }
	});

	//Initiat WOW JS
	new WOW().init();
	//smoothScroll
	smoothScroll.init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	$(document).ready(function() {
		//Animated Progress
		$('.progress-bar').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				$(this).css('width', $(this).data('width') + '%');
				$(this).unbind('inview');
			}
		});

		//Animated Number
		$.fn.animateNumbers = function(stop, commas, duration, ease) {
			return this.each(function() {
				var $this = $(this);
				var start = parseInt($this.text().replace(/,/g, ""));
				commas = (commas === undefined) ? true : commas;
				$({value: start}).animate({value: stop}, {
					duration: duration == undefined ? 1000 : duration,
					easing: ease == undefined ? "swing" : ease,
					step: function() {
						$this.text(Math.floor(this.value));
						if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
					},
					complete: function() {
						if (parseInt($this.text()) !== stop) {
							$this.text(stop);
							if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
						}
					}
				});
			});
		};

		$('.animated-number').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			var $this = $(this);
			if (visible) {
				$this.animateNumbers($this.data('digit'), false, $this.data('duration')); 
				$this.unbind('inview');
			}
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
        var formData = new FormData($(this)[0]);
		$.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            async: false,
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email en cours d\'envoi..</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">Merci pour votre intérêt.</p>').delay(3000).fadeOut();
            
		});
	});
    
    $('#uploadBtn').change(function() {
        var nom = $(this).val().replace("C:\\fakepath\\", "");
        var extension = nom.substr((nom.lastIndexOf('.') + 1));
        if(extension == "pdf" || extension == "docx") {
            $('#fileName').attr('value', nom);
            $('#fileName').stop().animate({
			     'border-color' : "#53DD49"},200);
        }else {
            $('#fileName').attr('value', 'Votre CV');
            $('#uploadBtn').wrap('<form>').closest('form').get(0).reset();
            $('#uploadBtn').unwrap();
            $('#fileName').stop().animate({
			     "border-color": "#e31818"},200);
        }
    });
    //formulaire postulation
    var form2 = $('#main-postule-form');
	form2.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
        var formData = new FormData($(this)[0]);
		$.ajax({
			url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            async: false,
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){
				form2.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> En cours d\'envoi...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">Merci pour votre postulation.</p>').delay(3000).fadeOut();
            form2.get(0).reset();
            $('#uploadBtn').wrap('<form>').closest('form').get(0).reset();
            $('#uploadBtn').unwrap();
            $('#fileName').attr('value', 'Votre CV');
            $('#fileName').stop().animate({
			     "border-color": "#ccc"},200);
            $('#tel').animate({
                "border-color":"#cccccc"},100);
            $('#postal-code').animate({
                "border-color":"#cccccc"},100);
		});
	});
    var able = [false, false];
    $('#tel').keyup(function() {
			var v = /^[0-9]{8}$/;
			if(!(v.test($(this).val()))) {
                able[0] = false;
                $(this).animate({
                "border-color":"#e31818"},100);
                if(able[0] && able[1]) {
                    $('#postuler').prop('disabled',false);
                }else {
                    $('#postuler').prop('disabled',true);
                }
            }else {
                able[0] = true;
                $(this).animate({
                "border-color":"#53DD49"},100);
                if(able[0] && able[1]) {
                    $('#postuler').prop('disabled',false);
                }else {
                    $('#postuler').prop('disabled',true);
                }
            }
    });
    $('#postal-code').keyup(function() {
			var v = /^[0-9]{4}$/;
			if(!(v.test($(this).val()))) {
                able[1] = false;
                $(this).animate({
                "border-color":"#e31818"},100);
                if(able[0] && able[1]) {
                    $('#postuler').prop('disabled',false);
                }else {
                    $('#postuler').prop('disabled',true);
                }
            }else {
                able[1] = true;
                $(this).animate({
                "border-color":"#53DD49"},100);
                if(able[0] && able[1]) {
                    $('#postuler').prop('disabled',false);
                }else {
                    $('#postuler').prop('disabled',true);
                }
            }
    });
	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});

	/* //Google Map
	var latitude = $('#google-map').data('latitude');
	var longitude = $('#google-map').data('longitude');
	function initMap() {
		var myLatlng = new google.maps.LatLng(latitude,longitude);
		var mapOptions = {
			zoom: 25,
			scrollwheel: false,
			center: myLatlng
		};
		var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map
		});
	} */

});
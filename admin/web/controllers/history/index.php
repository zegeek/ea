<?php

/*
 * This file is part of the CRUD Admin Generator project.
 *
 * Author: Jon Segador <jonseg@gmail.com>
 * Web: http://crud-admin-generator.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


require_once __DIR__.'/../../../vendor/autoload.php';
require_once __DIR__.'/../../../src/app.php';

use Symfony\Component\Validator\Constraints as Assert;





/* Download blob img */
$app->match('/history/download', function (Symfony\Component\HttpFoundation\Request $request) use ($app) { 
    
    // menu
    $rowid = $request->get('id');
    $idfldname = $request->get('idfld');
    $fieldname = $request->get('fldname');
    
    if( !$rowid || !$fieldname ) die("Invalid data");
    
    $find_sql = "SELECT " . $fieldname . " FROM " . history . " WHERE ".$idfldname." = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($rowid));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('menu_list'));
    }

    header('Content-Description: File Transfer');
    header('Content-Type: image/jpeg');
    header("Content-length: ".strlen( $row_sql[$fieldname] ));
    header('Expires: 0');
    header('Cache-Control: public');
    header('Pragma: public');
    ob_clean();    
    echo $row_sql[$fieldname];
    exit();
   
    
});



$app->match('/history', function (Symfony\Component\HttpFoundation\Request $request) use ($app) {

//    if(!$request->getSession()->get('admin')) {
//        return new Symfony\Component\HttpFoundation\Response(json_encode(), 403);
//    }

    $find_sql = "SELECT * FROM `history` WHERE hid=1";
    $row = $app['db']->fetchAll($find_sql, array())[0];

    return $app['twig']->render('history/list.html.twig', array(
        "history" => $row
    ));
        
})
->bind('history_list');




//$app->match('/history/create', function () use ($app) {
//
//    $initial_data = array(
//		'text' => '',
//
//    );
//
//    $form = $app['form.factory']->createBuilder('form', $initial_data);
//
//
//
//	$form = $form->add('text', 'textarea', array('required' => true));
//
//
//    $form = $form->getForm();
//
//    if("POST" == $app['request']->getMethod()){
//
//        $form->handleRequest($app["request"]);
//
//        if ($form->isValid()) {
//            $data = $form->getData();
//
//            $update_query = "INSERT INTO `history` (`text`) VALUES (?)";
//            $app['db']->executeUpdate($update_query, array($data['text']));
//
//
//            $app['session']->getFlashBag()->add(
//                'success',
//                array(
//                    'message' => 'history created!',
//                )
//            );
//            return $app->redirect($app['url_generator']->generate('history_list'));
//
//        }
//    }
//
//    return $app['twig']->render('history/create.html.twig', array(
//        "form" => $form->createView()
//    ));
//
//})
//->bind('history_create');



$app->match('/history/edit/{id}', function ($id) use ($app) {

    $find_sql = "SELECT * FROM `history` WHERE `hid` = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('history_list'));
    }

    
    $initial_data = array(
		'text' => $row_sql['text'], 

    );


    $form = $app['form.factory']->createBuilder('form', $initial_data);


	$form = $form->add('text', 'textarea', array('required' => true));


    $form = $form->getForm();

    if("POST" == $app['request']->getMethod()){

        $form->handleRequest($app["request"]);

        if ($form->isValid()) {
            $data = $form->getData();

            $update_query = "UPDATE `history` SET `text` = ? WHERE `hid` = ?";
            $app['db']->executeUpdate($update_query, array($data['text'], $id));            


            $app['session']->getFlashBag()->add(
                'success',
                array(
                    'message' => 'Historique modifié avec succès!',
                )
            );
            return $app->redirect($app['url_generator']->generate('history_edit', array("id" => $id)));

        }
    }

    return $app['twig']->render('history/edit.html.twig', array(
        "form" => $form->createView(),
        "id" => $id
    ));
        
})
->bind('history_edit');


//
//$app->match('/history/delete/{id}', function ($id) use ($app) {
//
//    $find_sql = "SELECT * FROM `history` WHERE `hid` = ?";
//    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));
//
//    if($row_sql){
//        $delete_query = "DELETE FROM `history` WHERE `hid` = ?";
//        $app['db']->executeUpdate($delete_query, array($id));
//
//        $app['session']->getFlashBag()->add(
//            'success',
//            array(
//                'message' => 'history deleted!',
//            )
//        );
//    }
//    else{
//        $app['session']->getFlashBag()->add(
//            'danger',
//            array(
//                'message' => 'Row not found!',
//            )
//        );
//    }
//
//    return $app->redirect($app['url_generator']->generate('history_list'));
//
//})
//->bind('history_delete');
//
//
//




<?php

/*
 * This file is part of the CRUD Admin Generator project.
 *
 * Author: Jon Segador <jonseg@gmail.com>
 * Web: http://crud-admin-generator.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../../src/app.php';


require_once __DIR__.'/category/index.php';
require_once __DIR__.'/col_prod/index.php';
require_once __DIR__.'/color/index.php';
require_once __DIR__.'/history/index.php';
require_once __DIR__.'/notification/index.php';
require_once __DIR__.'/product/index.php';
require_once __DIR__.'/service/index.php';
require_once __DIR__.'/staff/index.php';
require_once __DIR__.'/subcategory/index.php';



$app->match('/', function () use ($app) {

    return $app['twig']->render('ag_dashboard.html.twig', array());
        
})
->bind('dashboard');


$app->run();
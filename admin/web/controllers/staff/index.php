<?php

/*
 * This file is part of the CRUD Admin Generator project.
 *
 * Author: Jon Segador <jonseg@gmail.com>
 * Web: http://crud-admin-generator.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


require_once __DIR__.'/../../../vendor/autoload.php';
require_once __DIR__.'/../../../src/app.php';

use Symfony\Component\Validator\Constraints as Assert;

$app->match('/staff/list', function (Symfony\Component\HttpFoundation\Request $request) use ($app) {  
    $start = 0;
    $vars = $request->query->all();
    $qsStart = (int)$vars["start"];
    $search = $vars["search"];
    $order = $vars["order"];
    $columns = $vars["columns"];
    $qsLength = (int)$vars["length"];    
    
    if($qsStart) {
        $start = $qsStart;
    }    
	
    $index = $start;   
    $rowsPerPage = $qsLength;
       
    $rows = array();
    
    $searchValue = $search['value'];
    $orderValue = $order[0];
    
    $orderClause = "";
    if($orderValue) {
        $orderClause = " ORDER BY ". $columns[(int)$orderValue['column']]['data'] . " " . $orderValue['dir'];
    }
    
    $table_columns = array(
		'sid', 
		'name', 
		'position', 

    );
    
    $table_columns_type = array(
		'int(11)', 
		'varchar(150)', 
		'varchar(50)', 

    );    
    
    $whereClause = "";
    
    $i = 0;
    foreach($table_columns as $col){
        
        if ($i == 0) {
           $whereClause = " WHERE";
        }
        
        if ($i > 0) {
            $whereClause =  $whereClause . " OR"; 
        }
        
        $whereClause =  $whereClause . " " . $col . " LIKE '%". $searchValue ."%'";
        
        $i = $i + 1;
    }
    
    $recordsTotal = $app['db']->executeQuery("SELECT * FROM `staff`" . $whereClause . $orderClause)->rowCount();
    
    $find_sql = "SELECT * FROM `staff`". $whereClause . $orderClause . " LIMIT ". $index . "," . $rowsPerPage;
    $rows_sql = $app['db']->fetchAll($find_sql, array());

    foreach($rows_sql as $row_key => $row_sql){
        for($i = 0; $i < count($table_columns); $i++){

		if( $table_columns_type[$i] != "blob") {
				$rows[$row_key][$table_columns[$i]] = $row_sql[$table_columns[$i]];
		} else {				if( !$row_sql[$table_columns[$i]] ) {
						$rows[$row_key][$table_columns[$i]] = "0 Kb.";
				} else {
						$rows[$row_key][$table_columns[$i]] = " <a target='__blank' href='menu/download?id=" . $row_sql[$table_columns[0]];
						$rows[$row_key][$table_columns[$i]] .= "&fldname=" . $table_columns[$i];
						$rows[$row_key][$table_columns[$i]] .= "&idfld=" . $table_columns[0];
						$rows[$row_key][$table_columns[$i]] .= "'>";
						$rows[$row_key][$table_columns[$i]] .= number_format(strlen($row_sql[$table_columns[$i]]) / 1024, 2) . " Kb.";
						$rows[$row_key][$table_columns[$i]] .= "</a>";
				}
		}

        }
    }    
    
    $queryData = new queryData();
    $queryData->start = $start;
    $queryData->recordsTotal = $recordsTotal;
    $queryData->recordsFiltered = $recordsTotal;
    $queryData->data = $rows;
    
    return new Symfony\Component\HttpFoundation\Response(json_encode($queryData), 200);
});




/* Download blob img */
$app->match('/staff/download', function (Symfony\Component\HttpFoundation\Request $request) use ($app) { 
    
    // menu
    $rowid = $request->get('id');
    $idfldname = $request->get('idfld');
    $fieldname = $request->get('fldname');
    
    if( !$rowid || !$fieldname ) die("Invalid data");
    
    $find_sql = "SELECT " . $fieldname . " FROM " . staff . " WHERE ".$idfldname." = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($rowid));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('menu_list'));
    }

    header('Content-Description: File Transfer');
    header('Content-Type: image/jpeg');
    header("Content-length: ".strlen( $row_sql[$fieldname] ));
    header('Expires: 0');
    header('Cache-Control: public');
    header('Pragma: public');
    ob_clean();    
    echo $row_sql[$fieldname];
    exit();
   
    
});



$app->match('/staff', function () use ($app) {
    
	$table_columns = array(
		'name', 
		'position', 

    );

    $primary_key = "sid";
    $id="sid";

    return $app['twig']->render('staff/list.html.twig', array(
    	"table_columns" => $table_columns,
        "primary_key" => $primary_key,
        "id" => $id
    ));
        
})
->bind('staff_list');



$app->match('/staff/create', function () use ($app) {
    
    $initial_data = array(
		'name' => '', 
		'position' => '', 

    );

    $form = $app['form.factory']->createBuilder('form', $initial_data);



	$form = $form->add('name', 'text', array('required' => true));
	$form = $form->add('position', 'text', array('required' => true));


    $form = $form->getForm();

    if("POST" == $app['request']->getMethod()){

        $form->handleRequest($app["request"]);

        $find_sql = "SELECT count(sid) as nb FROM `staff`";
        $nb = $app['db']->fetchAll($find_sql, array())[0]['nb'];

        if ($form->isValid() && $nb<5) {
            $data = $form->getData();

            $update_query = "INSERT INTO `staff` (`name`, `position`) VALUES (?, ?)";
            $app['db']->executeUpdate($update_query, array($data['name'], $data['position']));            


            $app['session']->getFlashBag()->add(
                'success',
                array(
                    'message' => 'Membre du staff ajouté avec succès!',
                )
            );
            return $app->redirect($app['url_generator']->generate('staff_list'));

        }else{
            $app['session']->getFlashBag()->add(
                'danger',
                array(
                    'message' => 'Vous avez atteint le nombre maximal de membre (5)!',
                )
            );
            return $app->redirect($app['url_generator']->generate('staff_list'));
        }
    }

    return $app['twig']->render('staff/create.html.twig', array(
        "form" => $form->createView()
    ));
        
})
->bind('staff_create');



$app->match('/staff/edit/{id}', function ($id) use ($app) {

    $find_sql = "SELECT * FROM `staff` WHERE `sid` = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('staff_list'));
    }

    
    $initial_data = array(
		'name' => $row_sql['name'], 
		'position' => $row_sql['position'], 

    );


    $form = $app['form.factory']->createBuilder('form', $initial_data);


	$form = $form->add('name', 'text', array('required' => true));
	$form = $form->add('position', 'text', array('required' => true));


    $form = $form->getForm();

    if("POST" == $app['request']->getMethod()){

        $form->handleRequest($app["request"]);

        if ($form->isValid()) {
            $data = $form->getData();

            $update_query = "UPDATE `staff` SET `name` = ?, `position` = ? WHERE `sid` = ?";
            $app['db']->executeUpdate($update_query, array($data['name'], $data['position'], $id));            


            $app['session']->getFlashBag()->add(
                'success',
                array(
                    'message' => 'Profil du membre du staff modifié avec succès!',
                )
            );
            return $app->redirect($app['url_generator']->generate('staff_edit', array("id" => $id)));

        }
    }

    return $app['twig']->render('staff/edit.html.twig', array(
        "form" => $form->createView(),
        "staff" => $row_sql
    ));
        
})
->bind('staff_edit');



$app->match('/staff/delete/{id}', function ($id) use ($app) {

    $find_sql = "SELECT * FROM `staff` WHERE `sid` = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

    if($row_sql){
        $delete_query = "DELETE FROM `staff` WHERE `sid` = ?";
        $app['db']->executeUpdate($delete_query, array($id));

        $app['session']->getFlashBag()->add(
            'success',
            array(
                'message' => 'Membre du staff supprimé avec succès!',
            )
        );
    }
    else{
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );  
    }

    return $app->redirect($app['url_generator']->generate('staff_list'));

})
->bind('staff_delete');







-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2017 at 03:18 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ea`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `catid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `colid` int(11) NOT NULL,
  `color` varchar(50) NOT NULL,
  `code` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `col_prod`
--

CREATE TABLE `col_prod` (
  `cpid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `colid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `hid` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`hid`, `text`) VALUES
(1, '<p>\r\n<b>Quisque</b> vehicula metus eget viverra gravida. Vivamus bibendum a ante at \r\nlacinia. Vestibulum sed malesuada arcu. Ut elit mi, feugiat a leo sit \r\namet, consequat fringilla ligula. <b>Aenean</b> euismod libero at justo pretium\r\n eleifend. Vivamus sed vehicula lacus. Quisque commodo nibh at ligula \r\nconsequat scelerisque. Ut dictum ligula non neque ultrices tincidunt. \r\nProin placerat nec orci vitae hendrerit. Pellentesque porta ullamcorper \r\nnunc, id pellentesque diam porttitor vitae. Interdum et malesuada <b><i>fames</i></b> \r\nac ante ipsum primis in faucibus. Nam congue felis eget fringilla \r\ndapibus. Vestibulum mi metus, placerat a purus vel, aliquam suscipit \r\nipsum. Maecenas interdum et est sed placerat. Praesent dapibus cursus \r\nlorem, in aliquam odio.\r\n</p>\r\n<p>\r\nProin ornare arcu vel arcu mollis, sit amet pulvinar leo consequat. Nam \r\ntincidunt a felis ut eleifend. Maecenas a dignissim felis. Vivamus \r\nconsequat accumsan eros quis ultrices. Phasellus ornare iaculis velit \r\nvel dignissim. Donec lacinia sapien mauris, quis efficitur massa luctus \r\neu. Aliquam erat volutpat. Nunc non enim id turpis suscipit porttitor at\r\n eu ex. Etiam ante elit, lacinia ornare aliquam ut, placerat ac lacus. \r\nFusce sit amet dui quis justo commodo interdum. Morbi ut iaculis ante. \r\nMauris et justo ligula. Phasellus a mi sit amet tellus aliquam \r\nvenenatis.\r\n</p><br>');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `nid` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`nid`, `content`, `date`) VALUES
(1, 'Suspendisse et ligula id lectus scelerisque efficitur vitae sed ante. ', '2017-03-08 06:18:31'),
(2, 'luctus a mauris. Etiam et ante justo.', '2017-03-07 04:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pid` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `subcid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `serid` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`serid`, `text`) VALUES
(1, '<b><i>Vestibulum venenatis, lacus sit amet sempe</i></b>r rhoncus, risus dui congue \r\ndui, ac ultrices diam libero non odio. Nam sit amet pretium leo. Nullam \r\nfermentum nisl imperdiet dolor volutpat finibus. Donec sodales nunc sit \r\namet dolor condimentum, ut lobortis felis consequat. Cras varius non \r\nneque eu pulvinar. Vivamus consequat lectus diam, id ornare massa \r\nmolestie quis. Donec sem lacus, condimentum non scelerisque eu, faucibus\r\n a justo. Nunc at justo lacinia tellus varius sollicitudin. Pellentesque\r\n semper auctor dapibus.');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `sid` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `position` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`sid`, `name`, `position`) VALUES
(1, 'James', 'Developer'),
(2, 'Chandler', 'WENUS'),
(3, 'Fathi', 'GRH'),
(4, 'Hamdi', 'Responsable achat'),
(5, 'Arthur', 'CEO');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `subcid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `catid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`colid`);

--
-- Indexes for table `col_prod`
--
ALTER TABLE `col_prod`
  ADD PRIMARY KEY (`cpid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `colid` (`colid`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`hid`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`nid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `subcid` (`subcid`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`serid`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`subcid`),
  ADD KEY `catid` (`catid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `colid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `col_prod`
--
ALTER TABLE `col_prod`
  MODIFY `cpid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `hid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `serid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `subcid` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `col_prod`
--
ALTER TABLE `col_prod`
  ADD CONSTRAINT `col_prod_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `col_prod_ibfk_2` FOREIGN KEY (`colid`) REFERENCES `color` (`colid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`subcid`) REFERENCES `subcategory` (`subcid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `subcategory_ibfk_1` FOREIGN KEY (`catid`) REFERENCES `category` (`catid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
